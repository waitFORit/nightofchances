<?php

use Zend\Mvc\Application;
use Zend\Stdlib\ArrayUtils;

// Retrieve configuration
$appConfig = require __DIR__ . '/application.config.php';
if (file_exists(__DIR__ . '/development.config.php')) {
    $appConfig = ArrayUtils::merge($appConfig, require __DIR__ . '/development.config.php');
}

$app = Application::init($appConfig);

$entityManager = $app->getServiceManager()->get('doctrine.entity_manager.orm_default');

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(
    $entityManager
);
