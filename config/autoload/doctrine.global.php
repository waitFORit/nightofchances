<?php

use Doctrine\Common;
use Doctrine\ORM;

return [
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => Doctrine\DBAL\Driver\Mysqli\Driver::class,
                'params' => [
                    'host' => 'mariadb:3306',
                    'user' => 'user',
                    'password' => 'passwd',
                    'dbname' => 'db',
                ],
            ],
        ],

        'configuration' => [
            'orm_default' => [
                'naming_strategy' => ORM\Mapping\UnderscoreNamingStrategy::class,
                'auto_generate_proxy_classes' => false,
            ],
        ],

        'driver' => [
            'orm_default' => [
                'class' => Common\Persistence\Mapping\Driver\MappingDriverChain::class,
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            'doctrine.entity_manager.orm_default' => ContainerInteropDoctrine\EntityManagerFactory::class
        ],
        'invokables' => [
            ORM\Mapping\UnderscoreNamingStrategy::class => ORM\Mapping\UnderscoreNamingStrategy::class,
        ],
    ],
];
