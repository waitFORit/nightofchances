<?php
namespace Application\Controller;

use Application\Domain\Model\PollReply;
use Application\Domain\Model\PollReplyRepository;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    /**
     * @var PollReplyRepository
     */
    private $pollReplyRepository;

    public function __construct(PollReplyRepository $pollReplyRepository)
    {
        $this->pollReplyRepository = $pollReplyRepository;
    }

    public function indexAction()
    {
        return new ViewModel();
    }
}
