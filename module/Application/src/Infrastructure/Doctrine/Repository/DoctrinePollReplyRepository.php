<?php
namespace Application\Infrastructure\Doctrine\Repository;

use Application\Domain\Model\PollReply;
use Application\Domain\Model\PollReplyRepository;
use Doctrine\ORM\EntityRepository;

class DoctrinePollReplyRepository extends EntityRepository implements PollReplyRepository
{
    public function add(PollReply $pollReply)
    {
        $this->_em->persist($pollReply);
    }

    public function save()
    {
        $this->_em->flush();
    }

}