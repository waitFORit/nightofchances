<?php

namespace Application\Factory\Controller;

use Application\Controller\IndexController;
use Application\Domain\Model\PollReply;
use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): IndexController
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entity_manager.orm_default');

        return new IndexController($entityManager->getRepository(PollReply::class));
    }
}
