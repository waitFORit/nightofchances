<?php
namespace Application\Domain\Model;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Application\Infrastructure\Doctrine\Repository\DoctrinePollReplyRepository")
 */
class PollReply
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $option;

    /**
     * @ORM\Column(type="date")
     * @var DateTime
     */
    private $dateCreated;

    public static function create($name, $email, $option)
    {
        return new self(new DateTime(), $name, $email, $option);
    }

    private function __construct(DateTime $date, string $name, string $email, string $option)
    {
        $this->name = $name;
        $this->email = $email;
        $this->option = $option;
        $this->dateCreated = $date;

    }

}