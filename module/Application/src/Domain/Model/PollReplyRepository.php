<?php
namespace Application\Domain\Model;


interface PollReplyRepository
{
    public function add(PollReply $pollReply);
    public function save();
}