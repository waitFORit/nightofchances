<?php
namespace RestApi;

use RestApi;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Doctrine\ORM\Mapping\Driver;

return [
    'router' => [
        'routes' => [
            'poll-reply' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/rest-api/poll-reply[/:action]',
                    'defaults' => [
                        'controller' => Controller\PollReplyController::class,
//                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
           Controller\PollReplyController::class => Factory\Controller\PollReplyControllerFactory::class
        ],
    ],
    
    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
