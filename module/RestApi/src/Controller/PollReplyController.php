<?php
namespace RestApi\Controller;

use Application\Domain\Model\PollReply;
use Application\Domain\Model\PollReplyRepository;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Http\Response;
use Zend\View\Model\JsonModel;

class PollReplyController extends AbstractRestfulController
{
    /**
     * @var PollReplyRepository
     */
    private $pollReplyRepository;

    public function __construct(PollReplyRepository $pollReplyRepository)
    {
        $this->pollReplyRepository = $pollReplyRepository;
    }

    public function create($data)
    {
        $newPollReply = PollReply::create($data['name'], $data["email"], $data['option']);
        $this->pollReplyRepository->add($newPollReply);

        $this->pollReplyRepository->save();

        /** @var Response $response */
        $response = $this->getResponse();
        $response->setStatusCode(Response::STATUS_CODE_201);

        $result = new JsonModel(array(
            'message' => 'poll reply created',
            'success' => true,
        ));

        return $result;
    }
}
