<?php

namespace RestApi\Factory\Controller;


use Application\Domain\Model\PollReply;
use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use RestApi\Controller\PollReplyController;
use Zend\ServiceManager\Factory\FactoryInterface;

class PollReplyControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PollReplyController
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.entity_manager.orm_default');

        return new PollReplyController($entityManager->getRepository(PollReply::class));
    }
}
